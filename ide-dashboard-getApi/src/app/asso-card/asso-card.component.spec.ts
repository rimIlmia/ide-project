import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssoCardComponent } from './asso-card.component';

describe('AssoCardComponent', () => {
  let component: AssoCardComponent;
  let fixture: ComponentFixture<AssoCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssoCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssoCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
