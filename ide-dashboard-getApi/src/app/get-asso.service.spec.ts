import { TestBed } from '@angular/core/testing';

import { GetAssoService } from './get-asso.service';

describe('GetAssoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GetAssoService = TestBed.get(GetAssoService);
    expect(service).toBeTruthy();
  });
});
