import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { association } from './model/association';

@Injectable({
	providedIn: 'root'
})
export class GetAssoService {
	APIadress = 'http://localhost:3000/';
	data: any;
	constructor(private httpClient: HttpClient) {}

	getAllAsso(): Observable<association[]> {
		return this.httpClient.get<association[]>(`${this.APIadress}dashboard`);
	}

	getData() {
		return this.httpClient.get(`${this.APIadress}dashboard`).toPromise();
	}

	deleteAsso(id: string):Observable<association[]> {
		return this.httpClient.delete<association[]>(`${this.APIadress}table/${id}`);
	}
}
